
Sheet = Struct.new(:by_file) do
  include Enumerable

  def self.add(rows)
    new(rows.to_h { |row| [row.file, row] })
  end

  def -(other)
    self.class.new(
      by_file.merge(other.by_file) { |file, row1, row2| row1 - row2 }
    )
  end

  def each(&block)
    by_file.values.each(&block)
  end

  def size
    by_file.size
  end

  def sync_keys!(other)
    h1 = other.by_file.select { |k, _v| by_file.key?(k) }
    h2 = by_file.select { |k, _v| other.by_file.key?(k) }

    self.by_file = h2
    other.by_file = h1
  end
end

Row = Struct.new(:file, :duration, :query_count) do
  def self.from_csv(file, duration, query_count)
    new(file, duration.to_f, query_count.to_i)
  end

  def -(other)
    self.class.new(file, self.duration - other.duration, self.query_count - other.query_count)
  end
end

def parse(file)
  Sheet.add(
    File.readlines(file, chomp: true).map do |line|
      cols = *line.split(/\t/).first(3)
      Row.from_csv(*cols)
    end
  )
end

def summary(sheet)
  { query_count: sheet.sum(&:query_count), duration: sheet.sum(&:duration) }
end

before = parse("before.txt")
after = parse("after.txt")

before.sync_keys!(after)

diff = after - before

p before: summary(before)
p after: summary(after)
p diff: summary(diff)

p minmax_query: diff.minmax_by(&:query_count)
p minmax_duration: diff.minmax_by(&:duration)
