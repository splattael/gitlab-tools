
echo "URL:"
kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
echo

echo "CERT"
kubectl get secrets 
default=$(kubectl get secrets | grep default | awk '{print $1}')
kubectl get secrets "$default" -o go-template='{{index .data "ca.crt" | base64decode}}' 
echo

kubectl apply -f - <<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab-admin
    namespace: kube-system
EOF

echo "TOKEN"
kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') -o go-template='{{index .data "token" | base64decode}}'

echo
