#!/usr/bin/env ruby

require 'gitlab'
require 'gitlab-client-cache'
require 'parallel'

# List failed jobs due to timeout (> 90 minutes).
# Example usage:
#
# # Print top 5 examples which timedout
# bin/ci_job_timeouts.rb gitlab-org/gitlab "rspec.*" > timedout.txt
#
# <CTRL+C> when enough
#
# rg -I -o -r '$1' '(.*) ###' timedout.txt | sort | uniq -c | sort -nr | head -5

project, job_pattern, timeout = ARGV

timeout = timeout ? Integer(timeout) : 90
job_pattern = Regexp.compile(job_pattern || '.*')
timeout *= 60

warn "Searching for failed jobs with timeout > #{timeout}s matching `#{job_pattern}` in #{project}..."

abort "usage: #{$0} <project> [<job name pattern>] [<timeout in minutes>]" unless project

def example_timedout(trace)
  unless trace
    warn 'WARNING: This job does not have a trace.'
    return nil
  end

  trace = clean_trace(trace)

  start_index = trace.rindex(/^\w/)
  unless start_index
    warn 'WARNING: No starting line found'
    return nil
  end

  trace[start_index..-1]
end

def clean_trace(string)
  string
    .gsub(/\e\[(?:\d+.*?m)/, '') # escape ASCII
    .delete("\r")
    .gsub(/^section_\w+:.*$/, '')
end

def fold_description(trace)
  description = []

  depth = nil

  parts = trace.split("\n").reverse

  parts.filter_map do |part|
    if depth.nil? || part_depth(part) < depth
      depth = part_depth(part)
      part.lstrip
    end
  end.reverse.join(' ')
end

def part_depth(part)
  part.length - part.lstrip.length
end

counter = 0

Gitlab.jobs(project, scope: 'failed', per_page: 20).auto_paginate do |job|
  warn "# At #{job.finished_at}" if counter % 1000 == 0
  counter += 1

  next unless job.duration
  next if job.duration < timeout

  warn "## Timeout job=#{job.web_url} at=#{job.finished_at} name=`#{job.name}` duration=#{job.duration}"

  trace = Gitlab.job_trace(project, job.id)

  clean_trace = example_timedout(trace)
  next unless clean_trace

  puts "#{fold_description(clean_trace)} ### #{job.web_url}"
  $stdout.flush
end
