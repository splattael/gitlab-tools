#!/usr/bin/env ruby
# frozen_string_literal: true

require "gitlab"
require "gitlab-client-cache"
require "parallel"

file = ARGV.first or abort "usage: #$0 log.txt"

Counter = Struct.new(:count, :sum, :map) do
  def initialize
    super(0, 0, Hash.new(0))
  end

  def avg
    return 0 if count == 0

    sum / count.to_f
  end

  def start(job_name, timestamp)
    raise "Already started" if @started

    @job_name = job_name
    @started = timestamp
  end

  def stop(timestamp)
    raise "Not started" unless @started

    self.sum += timestamp - @started
    self.count += 1

    self.map[@job_name] += timestamp - @started

    @started = nil
  end
end

stat = {
  download: Counter.new,
  upload: Counter.new
}

lines = File.readlines(file)

jobs = lines.filter_map { |line| $1 if %r{## JOB: https://gitlab.com/gitlab-org/gitlab/-/jobs/(\d+)}.match(line) }
job_names = Parallel.map(jobs, in_threads: 8) do |job_id|
  job = Gitlab.job("gitlab-org/gitlab", job_id)
  [job_id, job.name]
end.to_h

job_name = nil

lines.each do |line|
  job_name = job_names.fetch($1) if %r{## JOB: https://gitlab.com/gitlab-org/gitlab/-/jobs/(\d+)}.match(line)

  case line
  when /section_start:(\d+):download_artifacts/
    stat[:download].start(job_name, Integer($1))
  when /^section_end:(\d+):download_artifacts/
    stat[:download].stop(Integer($1))
  when /section_start:(\d+):upload_artifacts/
    stat[:upload].start(job_name, Integer($1))
  when /^section_end:(\d+):upload_artifacts/
    stat[:upload].stop(Integer($1))
  end
end

p download: [stat[:download].sum, stat[:download].count, stat[:download].avg, stat[:download].map.sort_by { |_, x| -x }.first(3) ]
p upload: [stat[:upload].sum, stat[:upload].count, stat[:upload].avg, stat[:upload].map.sort_by { |_, x| -x }.first(3) ]
