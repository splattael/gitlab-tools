# frozen_string_literal: true

require "parallel"
require "open3"

class Files
  def initialize(glob, &block)
    @glob = glob
    @ignore = []
    instance_eval(&block)
  end

  def ignore(file)
    @ignore << file
  end

  def to_a
    reject_pattern = Regexp.union(@ignore)

    Dir[@glob].reject { |file| reject_pattern.match?(file) }
  end
end

class IgnoreWarnings
  def initialize(&block)
    @ignore = []
    instance_eval(&block)
  end

  def ignore(pattern)
    @ignore << pattern
  end

  def filter(warnings)
    warnings.reject do |warning|
      @ignore.any? { |pattern| pattern.match?(warning) }
    end
  end
end

files = Files.new("*/**/*.rb") do
  ignore %r{^tmp}
  ignore %r{^generator_templates}
end

ignore_warnings = IgnoreWarnings.new do
  ignore %r{^.*: warning: ambiguous first argument; put parentheses or a space even after `/' operator}
  ignore %r{^(ee/)?spec.*?: warning: possibly useless use of \| in void context}
end

Parallel.each(files.to_a) do |file|
  Open3.popen3("ruby -wc #{file}") do |_stdin, _stdout, stderr, _wait_thr|
    warnings = stderr
      .readlines
      .map(&:chomp)
      .then { |warnings| ignore_warnings.filter(warnings) }

    puts warnings unless warnings.empty?
  end
end
