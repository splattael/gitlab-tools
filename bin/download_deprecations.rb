#!/bin/bash

set -e

url="https://gitlab.com/api/v4/projects/278964/pipelines/${PIPELINE_ID}/jobs?per_page=100&scope=success"

curl "${url}&page=1" > jobs1.txt
curl "${url}&page=2" > jobs2.txt
curl "${url}&page=3" > jobs3.txt

ruby -e "require 'json'; puts JSON.parse(File.read('jobs1.txt')).select{|i| i['name'].include?('rspec')}.map{|i| i['id']}" > job_ids.txt
ruby -e "require 'json'; puts JSON.parse(File.read('jobs2.txt')).select{|i| i['name'].include?('rspec')}.map{|i| i['id']}" >> job_ids.txt
ruby -e "require 'json'; puts JSON.parse(File.read('jobs3.txt')).select{|i| i['name'].include?('rspec')}.map{|i| i['id']}" >> job_ids.txt

set +e
while IFS= read -r job_id; do wget "https://gitlab.com/api/v4/projects/278964/jobs/${job_id}/artifacts" ; done < job_ids.txt

yes | unzip "artifacts*" deprecations/*
