#!/usr/bin/env ruby

require 'csv'
require 'set'

# See https://gitlab.com/-/snippets/2020566

raise "usage: $0 <warning to match>" if ARGV.empty?

match = Regexp.new(ARGV[0])

files = Dir.glob('*.csv')

warnings = Hash.new { |h, k| h[k] = Set.new }

def unquote(string)
  string&.gsub(/^"|"$/, '')
end

files.each do |file|
  data = CSV.read(file, liberal_parsing: true)

  data.each do |line|
    warning = line[0]&.strip
    source = unquote(line[1]&.strip)

    next if source == ':'
    next unless match.match?(warning)

    target = warning[/(.*): warning:/, 1]

    # Same? Let's group by file
    if target == source
      target = target[/(.*?):/, 1]
    end

    warnings[target] << source
  end
end

puts "## #{ARGV[0]}"
puts

warnings.sort.each do |target, sources|
  puts "- [ ] `#{target}`"
  sources.each do |source|
    puts "   * `#{source}`"
  end
end
