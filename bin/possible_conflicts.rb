# frozen_string_literal: true

require "rugged"

class Searcher
  attr_reader :repo

  def initialize(repo, file_pattern:, content_pattern:)
    @repo = repo
    @file_pattern = file_pattern
    @content_pattern = content_pattern
  end

  def diff(branch)
    c1 = branch.target
    c2 = c1

    loop do
      break if c2.parents.size > 1

      c2 = c2.parents[0]
    end

    #p branch.name => [c1.oid, c2.oid]

    diff = c2.diff(c1)

    diff.each_patch do |patch|
      file = patch.delta.new_file[:path]
      next unless @file_pattern.match?(file)

      patch.each_hunk do |hunk|
        hunk.each_line do |line|
          next unless line.addition?
          next unless @content_pattern.match?(line.content)

          yield(file, line)
        end
      end
    end
  end
end

class BranchList
  attr_reader :repo

  FITLER_BY_NAME = proc do |branch|
    !/\bmaster$/.match?(branch.name) && %r{origin/}.match?(branch.name)
  end

  SORT_BY_DATE_DESC = proc { |branch| branch.target.respond_to?(:author) ? -branch.target.author[:time].to_f : -1 }

  def initialize(repo)
    @repo = repo
  end

  def each_branch(time_ago, &block)
    upto = Time.now - time_ago

    @repo
      .branches
      .lazy
      .select(&FITLER_BY_NAME)
      .sort_by(&SORT_BY_DATE_DESC)
      .take_while { |branch| branch.target.author[:time] >= upto }
      .each(&block)
  end
end

class FileCount
  attr_reader :total, :by_branch

  def initialize(file)
    @file = file
    @total = 0
    @by_branch = Hash.new(0)
  end

  def per_branch(&block)
    @by_branch.sort_by { |_, count| count }.each(&block)
  end

  def inc(branch)
    @total += 1
    @by_branch[branch] += 1
  end
end

class Counter
  attr_reader :last_branch

  def initialize(searcher)
    @searcher = searcher
    @files = Hash.new { |hash, file| hash[file] = FileCount.new(file) }
    @last_branch = nil
  end

  def count(branch)
    @last_branch = branch
    @searcher.diff(branch) do |file, line|
      @files[file].inc(branch.name)
    end
  end

  def each_files(&block)
    @files.sort_by { |_, file_count| file_count.total }.each(&block)
  end
end

repo = Rugged::Repository.new(".")

searcher = Searcher.new(
  repo,
  file_pattern: %r{.rubocop_manual_todo.yml},
  content_pattern: %r{.*}
)

counter = Counter.new(searcher)

UPTO = 86400 * 30

BranchList
  .new(repo)
  .each_branch(UPTO, &counter.method(:count))

counter.each_files do |file, count|
  puts "#{file}: #{count.total}"
  count.per_branch do |branch, c|
    #puts "  #{branch}: #{c}"
  end
end

p counter.last_branch.target.author[:time].utc

