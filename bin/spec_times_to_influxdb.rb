# frozen_string_literal: true

require "json"
require "gitlab"
require "gitlab-client-cache"
require "influxdb-client"
require "parallel"

INFLUXDB_URL = "http://localhost:8086"
INFLUX_CI_JOB_METRICS_BUCKET= "spec_durations"

JOB_PATTERN = %r{^rspec.*}
JOB_TIMEOUT = 90 * 60

project, _ = ARGV

abort "usage: #{$0} <project>" unless project

def each_pipeline(project, &block)
  params = {
    scope: 'finished',
    ref: 'master',
    source: 'schedule',
    per_page: 50
  }

  Gitlab.pipelines(project, params).auto_paginate(&block)
end

def jobs_for(project, pipeline)
  Gitlab.pipeline_jobs(project, pipeline.id).auto_paginate.filter_map do |job|
    next unless JOB_PATTERN.match?(job.name)

    job
  end.to_a
end

def job_trace(project, job)
  retrying ||= 0
  Gitlab.job_trace(project, job.id)
rescue Gitlab::Error::InternalServerError => e
  retrying += 1

  if retrying > 3
    warn "  Giving up..."
    return nil
  end

  sleeping = 2 ** retrying
  warn "  ERROR #{e} - sleeping #{sleeping}s"
  sleep sleeping

  retry
end

def parse_json(title, trace)
  index = trace.index(title)

  unless index
    warn "WARNING: `#{title}` not found"
    return nil
  end

  start_brace = trace.index("{", index)

  unless start_brace
    warn "WARNING: { not found"
    return nil
  end

  end_brace = trace.index("}", start_brace)

  unless end_brace 
    warn "WARNING: } not found"
    return nil
  end

  part = trace[start_brace..end_brace]

  JSON.parse(part)
rescue JSON::ParserError
  warn "WARNING: Failed to parse JSON of `#{part[0, 100]}...`"
  nil
end

def expected_duration(trace)
  parse_json('Expected duration for tests', trace)
end

def actual_duration(trace)
  parse_json('Knapsack report was generated. Preview:', trace)
end

def write_to_influxdb(job, expected, actual)
  common_tags = {
    job_id: job.id,
    job_status: job.status,
  }

  common_tags[:job_timeout] = 1 if (job.duration || 0) > JOB_TIMEOUT ? 1 : 0

  time = Time.parse(job.created_at).to_i

  common_data = {
    name: 'spec_runtime',
    time: time
  }

  data = []

  actual.each do |file, actual_duration|
    expected_duration = expected[file]

    tags = common_tags.merge(tags_from_file(file))
    fields = { actual: actual_duration }
    fields[:expected] = expected_duration if expected_duration

    data << common_data.merge(tags: tags, fields: fields)
  end

  influx_write_api.write(data: data)
end

def tags_from_file(file)
  ee = file.start_with?("ee/")
  spec_type = file.gsub(/^ee\//, "").gsub(/^spec\//, "").split("/").first

  {
    file: file,
    edition: ee ? 'ee' : 'ce',
    spec_type: spec_type
  }
end

def influx_write_api
  @write_api ||= influx_client.create_write_api
end

def influx_client
  @influx_client ||= InfluxDB2::Client.new(
    INFLUXDB_URL,
    ENV["INFLUXDB_TOKEN"] || raise("Missing INFLUXDB_TOKEN env variable"),
    bucket: INFLUX_CI_JOB_METRICS_BUCKET,
    org: "gitlab",
    precision: InfluxDB2::WritePrecision::SECOND,
    use_ssl: false
  )
end

each_pipeline(project) do |pipeline|
  warn "### Pipeline #{pipeline.id} at=#{pipeline.created_at} url=#{pipeline.web_url}"

  jobs = jobs_for(project, pipeline)
  Parallel.each(jobs) do |job|
    warn "### Job #{job.name} at=#{pipeline.created_at} url=#{job.web_url}"
    trace = job_trace(project, job)

    unless trace
      warn "  No trace"
      next
    end

    actual = actual_duration(trace) || {}
    expected = expected_duration(trace) || {}

    if actual.empty? && expected.empty?
      warn "  No timings"
      next
    end

    write_to_influxdb(job, expected, actual)
  end
end
