#!/usr/bin/env ruby

abort "#$0 <rails-root> <CODEOWNERS file>" unless ARGV.size == 2

root_dir, glob = *ARGV

File.readlines(glob).each do |line|
  pattern = line[/(\S+)\s*@.*/, 1]
  next unless pattern

  glob = pattern.start_with?('/') ? pattern.dup : "/**/#{pattern}"
  glob = glob.prepend(root_dir)

  if Dir.glob(glob).empty?
    puts pattern
  end
end

