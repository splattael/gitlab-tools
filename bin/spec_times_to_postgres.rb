# frozen_string_literal: true

require "json"
require "gitlab"
require "gitlab-client-cache"
require "pg"
require "parallel"
require "active_record"
require "thread"

Gitlab::Client::Cache.configure do |config|
  config.expires_in = [
    ->(path, _) { %r{/pipelines($|\?)}.match?(path) ? :skip_caching : :default },
    ->(path, _) { path.end_with?('/trace') ? :skip_caching : :default },
    nil
  ]
end

trace_cache_config = Gitlab::Client::Cache::Config.build
trace_cache_config.scope = 'spec_times_to_postgres_traces'
trace_cache_config.expires_in = nil
TRACE_CACHE = Gitlab::Client::Cache::Caching.new(trace_cache_config)

ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  encoding: 'unicode',
  host: '10.23.0.5',
  database: 'spec_runtimes',
  pool: 20
)

def create_schema
  spec_type_names= %w[
    features
    requests
    controllers
    mailers
    models
    services
    lib
    tasks
    replicators
    finders
    serializers
    workers
    graphql
    views
    components
    helpers
    validators
    presenters
    policies
    routing
    elastic
    channels
    elastic_integration
    experiments
    initializers
    db
    config
    support_specs
    uploaders
    frontend
    bin
    migrations
    click_house
    commands
    metrics_server
    rubocop
    tooling
    scripts
    dependencies
    haml_lint
    spam
    sidekiq_cluster
    contracts
    rack_servers
    sidekiq
    keeps
    dot_gitlab_ci
  ]

  ActiveRecord::Schema.define do
    create_table :ci_pipelines do |t|
      t.string :name, null: false
      t.string :ref, null: false
      t.datetime :created_at, null: false
      t.datetime :scanned_at, null: true
    end

    create_table :ci_jobs do |t|
      t.references :ci_pipeline, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.string :name, null: false
      t.integer :job_status, null: false, limit: 2
      t.datetime :started_at, null: false
      t.datetime :finished_at, null: true
    end

    create_table :spec_types do |t|
      t.string :name, null: false
      t.index :name, unique: true
    end

    create_table :spec_files do |t|
      t.string :name, null: false
      t.index :name, unique: true
      t.references :spec_type, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.integer :edition, null: false, limit: 1
    end

    create_table :spec_actual_runtimes, primary_key: [:ci_job_id, :spec_file_id] do |t|
      t.references :ci_job, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.references :spec_file, null: false, foreign_key: { on_delete: :cascade }
      t.integer :duration, null: false
    end

    create_table :spec_expect_runtimes, primary_key: [:ci_job_id, :spec_file_id] do |t|
      t.references :ci_job, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.references :spec_file, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.integer :duration, null: false
    end

    create_table :spec_loadavgs, primary_key: [:ci_job_id, :spec_file_id] do |t|
      t.references :ci_job, null: false, index: true, foreign_key: { on_delete: :cascade }
      t.references :spec_file, null: false, foreign_key: { on_delete: :cascade }
      t.integer :rss_mb, limit: 2
      t.integer :minute1, limit: 2
      t.integer :minute5, limit: 2
      t.integer :minute15, limit: 2
      t.integer :processes, limit: 2
      t.integer :threads, limit: 2
    end
  end

  spec_type_names.each do |name|
    SpecType.create!(name: name)
  end
end

class CiPipeline < ActiveRecord::Base
  def self.scanned
    where("scanned_at IS NOT NULL").order(created_at: :desc)
  end

  def self.pending
    where("scanned_at IS NULL").order(created_at: :desc)
  end
end

class CiJob < ActiveRecord::Base
  enum :job_status, [:success, :failed, :timeout, :unknown]
  belongs_to :ci_pipeline

  def self.from_job(job)
    name = job.name
    status = status_from(job)
    started_at = parse_time(job.started_at)
    finished_at = parse_time(job.finished_at) if job.finished_at

    new(
      id: job.id,
      name: name,
      job_status: status,
      started_at: started_at,
      finished_at: finished_at
    )
  end

  def self.status_from(job)
    return :timeout if job.duration > JOB_TIMEOUT

    case job.status
    when 'success'
      :success
    when 'failed'
      :failed
    else
      :unknown
    end
  end
end

class SpecType < ActiveRecord::Base
end

class SpecFile < ActiveRecord::Base
  enum :edition, [:ce, :ee]
  belongs_to :spec_type
end

class SpecActualRuntime < ActiveRecord::Base
  belongs_to :ci_job
  belongs_to :spec_file
end

class SpecExpectRuntime < ActiveRecord::Base
  belongs_to :ci_job
  belongs_to :spec_file
end

class SpecLoadavg < ActiveRecord::Base
  belongs_to :ci_job
  belongs_to :spec_file
end

create_schema unless CiPipeline.table_exists?

SPEC_TYPES = SpecType.all.index_by(&:name)

JOB_PATTERN = %r{^rspec.*}
JOB_TIMEOUT = 80 * 60

project, _ = ARGV

abort "usage: #{$0} <project>" unless project

def each_scheduled_pipeline(project, ref: [], &block)
  loop do
    latest_pipelines(project, ref: ref, &block)

    resume_pipelines(project, ref: ref, &block)

    older_pipelines(project, 10, ref: ref, &block)
  end
end

def latest_pipelines(project, ref:, &block)
  updated_at = CiPipeline.scanned.first&.created_at || (Time.now - 24 * 60 * 60)

  p NEWER: updated_at

  iterate_scheduled_pipelines(project, updated_after: updated_at) do |pipeline|
    if ref.empty? || ref.include?(pipeline.ref)
      yield pipeline
    end
  end
end

def resume_pipelines(project, ref:, &block)
  ids = CiPipeline.pending.pluck(:id)

  p RESUME: ids

  ids.each do |id|
    pipeline = Gitlab.pipeline(project, id)
    if ref.empty? || ref.include?(pipeline.ref)
      yield pipeline
    end
  end

end

def older_pipelines(project, limit, ref:, &block)
  updated_at = CiPipeline.scanned.last&.created_at || (Time.now - 24 * 60 * 60)

  amount = 0

  p OLDER: updated_at

  iterate_scheduled_pipelines(project, updated_before: updated_at) do |pipeline|
    if ref.empty? || ref.include?(pipeline.ref)
      amount += 1
      yield pipeline
      break if amount >= limit
    end
  end
end

def iterate_scheduled_pipelines(project, more_params = {}, &block)
  params = {
    scope: 'finished',
    source: 'schedule',
    per_page: 20
  }.merge(more_params)

  Gitlab.pipelines(project, params).auto_paginate(&block)
end

def each_pipeline(project, &block)
  params = {
    scope: 'finished',
    per_page: 20
  }

  Gitlab.pipelines(project, params).each_page do |page|
    pipelines = page.to_a.shuffle

    loop do
      pipeline = pipelines.shift
      was_new = yield pipeline
      break if was_new || pipelines.empty?
    end
  end
end

def jobs_for(project, pipeline)
  params = {
    scope: ['success', 'failed']
  }

  Gitlab.pipeline_jobs(project, pipeline.id, params).auto_paginate.filter_map do |job|
    next unless JOB_PATTERN.match?(job.name)

    job
  end.to_a
end

def job_trace(project, job)
  retrying ||= 0
  Gitlab.job_trace(project, job.id)
rescue Gitlab::Error::Forbidden
  warn "  WARNING - FORBIDDEN"
  nil
rescue Gitlab::Error::InternalServerError => e
  retrying += 1

  if retrying > 3
    warn "  Giving up..."
    return nil
  end

  sleeping = 2 ** retrying
  warn "  ERROR #{e} - sleeping #{sleeping}s"
  sleep sleeping

  retry
end

def parse_json(title, trace)
  index = trace.index(title)

  unless index
    warn "WARNING: `#{title}` not found"
    return nil
  end

  start_brace = trace.index("{", index)

  unless start_brace
    warn "WARNING: { not found"
    return nil
  end

  end_brace = trace.index("}", start_brace)

  unless end_brace 
    warn "WARNING: } not found"
    return nil
  end

  part = trace[start_brace..end_brace]

  JSON.parse(part)
rescue JSON::ParserError
  warn "WARNING: Failed to parse JSON of `#{part[0, 100]}...`"
  nil
end

def expected_duration(trace)
  parse_json('Expected duration for tests', trace)
end

def actual_duration(trace)
  parse_json('Knapsack report was generated. Preview:', trace)
end

def write(pipeline, job, expected, actual, loadavgs)
  return if CiJob.exists?(job.id)

  CiJob.transaction do
    job = CiJob.from_job(job)
    job.ci_pipeline = pipeline
    job.save!

    actual.each do |file, actual_duration|
      expected_duration = expected[file]
      spec_file = spec_file_for(file)

      SpecActualRuntime.create!(ci_job: job, spec_file: spec_file, duration: (actual_duration * 1000).to_i)
      SpecExpectRuntime.create!(ci_job: job, spec_file: spec_file, duration: (expected_duration * 1000).to_i) if expected_duration

      if (loadavg = loadavgs[file])
        SpecLoadavg.create!(loadavg) do |la|
          la.ci_job = job
          la.spec_file = spec_file
        end
      end
    end
  end
end

def spec_type_for(file)
  spec_type = file.gsub(/^ee\//, "").gsub(/^spec\//, "").split("/").first

  SPEC_TYPES.fetch(spec_type)
end

def spec_file_for(file)
  SpecFile.create_or_find_by!(name: file) do |f|
    f.spec_type = spec_type_for(file)
    f.edition = file.start_with?("ee/") ? :ee : :ce
  end
end

def parse_time(string)
  Time.parse(string).change(usec: 0)
end

def process_job(project, ci_pipeline, job)
  actual, expected, loadavg = extract_info(project, ci_pipeline, job)

  if actual.nil? || (actual.empty? && expected.empty?)
    warn "  No timings"
    return
  end

  write(ci_pipeline, job, expected, actual, loadavg)
end

def extract_info(project, ci_pipeline, job)
  version = "2"
  TRACE_CACHE.cached(project, ci_pipeline.id, job.id, version) do
    trace = job_trace(project, job)

    unless trace
      warn "  No trace"
      next
    end

    actual = actual_duration(trace) || {}
    expected = expected_duration(trace) || {}
    loadavgs = extract_loadavgs(trace) || {}

    [actual, expected, loadavgs]
  end
end

def extract_loadavgs(trace)
  last_se_index = 0

  loadavgs = {}

  loop do
    se_match = %r{Starting example group (?<spec_file>\S+)\.}.match(trace, last_se_index)
    break unless se_match
    last_se_index = se_match.offset(0).last
    spec_file = se_match.named_captures.fetch("spec_file")
    last_rss_index = last_se_index

    ee_match = %r{Finishing example group}.match(trace, last_se_index)
    ee_index = ee_match ? ee_match.offset(0).last : 0

    loop do
      rss_match = %r{Current RSS: ~(?<rss_mb>\d+)M. load average: (?<minute1>\d+\.\d+) (?<minute5>\d+\.\d+) (?<minute15>\d+\.\d+) (?<processes>\d+)/(?<threads>\d+)}.match(trace, last_rss_index)
      break unless rss_match

      break if rss_match.offset(0).last > ee_index

      last_rss_index = rss_match.offset(0).last

      loadavg = rss_match.named_captures
      loadavg["rss_mb"] = Integer(loadavg.fetch("rss_mb"))
      loadavg["minute1"] = (Float(loadavg.fetch("minute1")) * 100).to_i
      loadavg["minute5"] = (Float(loadavg.fetch("minute5")) * 100).to_i
      loadavg["minute15"] = (Float(loadavg.fetch("minute15")) * 100).to_i
      loadavg["processes"] = Integer(loadavg.fetch("processes"))
      loadavg["threads"] = Integer(loadavg.fetch("threads"))

      loadavgs[spec_file] = loadavg
    end
  end

  loadavgs
end

def process_pipeline(project, pipeline)
  ci_pipeline = CiPipeline.find_or_create_by!(id: pipeline.id) do |p|
    p.name = pipeline.name || '?'
    p.ref = pipeline.ref || '?'
    p.created_at = parse_time(pipeline.created_at)
  end

  return false if ci_pipeline.scanned_at

  jobs = jobs_for(project, pipeline)
  if jobs.size < 10
    ci_pipeline.update!(scanned_at: Time.now)
    return true
  end

  Parallel.each(jobs, in_threads: 16) do |job|
    warn "### Job #{job.name} at=#{pipeline.created_at} url=#{job.web_url} status=#{job.status}"

    process_job(project, ci_pipeline, job)
  end

  ci_pipeline.update!(scanned_at: Time.now)

  true
end

each_scheduled_pipeline(project, ref: %w[master ruby3_0 ruby3_2]) do |pipeline|
  warn "### Pipeline #{pipeline.id} at=#{pipeline.created_at} url=#{pipeline.web_url}"

  process_pipeline(project, pipeline)
end if $0 == __FILE__
