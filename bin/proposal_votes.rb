require "yaml"
require "json"
require "cgi"
require "open-uri"

class Awards
  URL = "https://gitlab.com/api/v4/projects/%{project_name}/issues/%{issue_id}/award_emoji?per_page=100"

  IS_UPVOTE = proc { |v| v["name"] == "thumbsup" }
  MAP_USERNAME = proc { |v| v.dig("user", "username") }

  attr_reader :votes

  def initialize(votes)
    @votes = votes
  end

  def voters
    @voters ||= votes.map(&MAP_USERNAME)
  end

  def upvoters
    @upvoters ||= votes.select(&IS_UPVOTE).map(&MAP_USERNAME)
  end

  def self.fetch(project_name, issue_id, private_token)
    project_name = CGI.escape(project_name)

    url = format(URL, project_name: project_name, issue_id: issue_id)

    open(url, "Private-Token" => private_token)
      .read
      .then(&JSON.method(:load))
      .then(&method(:new))
  end
end

class Team
  URL = "https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml"

  IS_MAINTAINER = proc { |m| role_of?(m, "gitlab", "maintainer backend") }
  MAP_USERNAME = proc { |m| m["gitlab"] }

  attr_reader :members

  def initialize(members)
    @members = members
  end

  def maintainers
    @maintainers ||= members.select(&IS_MAINTAINER).map(&MAP_USERNAME)
  end

  def self.fetch
    open(URL)
      .read
      .then(&YAML.method(:load))
      .then(&method(:new))
  end

  private_class_method def self.role_of?(member, project, role)
    Array(member.dig("projects", project)).include?(role)
  end
end

class Calculator
  def initialize(awards, team)
    @awards = awards
    @team = team
  end

  def maintainers_percent_weigh_in
    return 0 if maintainer_count.zero?

    round(maintainers_votes * 100.0 / maintainer_count)
  end

  def maintainers_percent_upvotes
    return 0 if maintainers_votes.zero?

    round(maintainers_upvotes * 100.0 / maintainers_votes)
  end

  def maintainer_count
    @team.maintainers.size
  end

  def maintainers_votes
    (@team.maintainers & @awards.voters).size
  end

  def maintainers_upvotes
    (@team.maintainers & @awards.upvoters).size
  end

  private

  def round(number)
    format("%.2f", number)
  end
end

issue_url = ARGV.first or abort "usage: $#0 issue_url"
private_token = ENV.fetch("GITLAB_API_PRIVATE_TOKEN") { abort "missing env GITLAB_API_PRIVATE_TOKEN" }

project_name, issue_id = issue_url.scan(%r{https://gitlab.com/(.*?)/issues/(\d+)}).first
abort "invalid issue_url" unless issue_id

awards = Awards.fetch(project_name, issue_id, private_token)
team = Team.fetch
calculator = Calculator.new(awards, team)

puts <<~TXT
Issue: #{issue_url}

Maintainer participation: #{calculator.maintainers_percent_weigh_in}% (#{calculator.maintainers_votes} of #{calculator.maintainer_count} maintainers)
Maintainer upvotes: #{calculator.maintainers_percent_upvotes}% (#{calculator.maintainers_upvotes} of #{calculator.maintainers_votes} votes)
TXT
